package lhz;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @Author: LiHuaZhi
 * @Date: 2021/10/20 21:30
 * @Description:
 **/
public class RabbitMqUtils {
    /**
     * 得到一个连接的  channel
     *
     * @return
     * @throws Exception
     */
    public static Channel getChannel() throws Exception {
        //创建一个连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("127.0.0.1");
        factory.setUsername("admin");
        factory.setPassword("admin");
        Connection connection = factory.newConnection();
        return connection.createChannel();
    }
}