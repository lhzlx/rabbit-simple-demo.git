package lhz.simple;

import com.rabbitmq.client.*;
import lhz.RabbitMqUtils;

/**
 * @Author: LiHuaZhi
 * @Date: 2021/10/20 20:30
 * @Description:
 **/
public class Consumer {
    /**
     * 设置队列名称
     */
    private final static String QUEUE_NAME = "hello";

    public static void main(String[] args) throws Exception {
        // 获取Channel
        Channel channel = RabbitMqUtils.getChannel();

        // 消费队列消息的一个回调接口
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody());
            System.out.println("消息消费成功,内容:");
            System.out.println(message);
        };
        // 取消消费的一个回调接口   如在消费的时候队列被删除掉了
        CancelCallback cancelCallback = (consumerTag) -> {
            System.out.println("消息消费被中断");
        };
        /*
         * 消费者消费消息
         * 1.消费哪个队列
         * 2.消费成功之后是否要自动应答   true代表自动应答   false手动应答
         * 3.消费者未成功消费的回调
         * 3.消费者取消消费的的回调
         */
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, cancelCallback);
        System.out.println("等待接收消息....");
    }
}
