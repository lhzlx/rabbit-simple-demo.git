package lhz.fanout;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import lhz.RabbitMqUtils;

/**
 * @Author: LiHuaZhi
 * @Date: 2021/10/20 20:30
 * @Description:
 **/
public class Producer {

    /**
     * 定义交换机和队列名称
     */
    private static final String EXCHANGE_NAME = "fanout_exchange";

    public static void main(String[] args) throws Exception {
        // 获取Channel
        Channel channel = RabbitMqUtils.getChannel();

        /*绑定的交换机 参数1交互机名称 参数2 exchange类型 */
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
        for (int i = 0; i < 10; i++) {
            String message = "消息：" + i;
            // 发送一个消息,1.发送到那个交换机,2.路由的  key是哪个,3.其他的参数信息,4.发送消息的消息体
            channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes());
        }
        System.out.println("消息发送完毕");
    }
}
