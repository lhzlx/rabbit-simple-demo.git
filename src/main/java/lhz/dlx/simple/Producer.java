package lhz.dlx.simple;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;
import lhz.RabbitMqUtils;

/**
 * @Author: LiHuaZhi
 * @Date: 2021/10/20 20:30
 * @Description:
 **/
public class Producer {
    /**
     * 设置队列名称
     */
    private static final String NORMAL_ROUTING_KEY = "normal";
    private static final String NORMAL_EXCHANGE = "normal_exchange";

    public static void main(String[] args) throws Exception {
        // 获取Channel
        Channel channel = RabbitMqUtils.getChannel();

        channel.exchangeDeclare(NORMAL_EXCHANGE, BuiltinExchangeType.DIRECT);

        AMQP.BasicProperties properties = null;
        // TODO 设置消息的TTL时间，模拟TTL过期的情况
        // TODO 如果是演示：消息拒绝、队列在达到最大长度等情况时，去掉设置TTL的代码
        // properties =new AMQP.BasicProperties().builder().expiration("10000").build();

        for (int i = 1; i < 11; i++) {
            String message = "info" + i;
            channel.basicPublish(NORMAL_EXCHANGE, NORMAL_ROUTING_KEY, properties, message.getBytes());
        }
        System.out.println("消息发送完毕");
    }
}
