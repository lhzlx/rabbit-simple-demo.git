package lhz.toptic;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import lhz.RabbitMqUtils;

/**
 * @Author: LiHuaZhi
 * @Date: 2021/10/20 20:30
 * @Description:
 **/
public class Producer {

    /**
     * 定义交换机和队列名称
     */
    private static final String EXCHANGE_NAME = "topic";

    public static void main(String[] args) throws Exception {
        // 获取Channel
        Channel channel = RabbitMqUtils.getChannel();

        /*绑定的交换机 参数1交互机名称 参数2 exchange类型 */
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);
        // 定义路由key
        String routingKey = "mq.info.log";
        String message = "topic_exchange_msg：" + routingKey;
        System.out.println("[send] = " + message);
        // 发送消息

        // 第二个参数就是路由键
        channel.basicPublish(EXCHANGE_NAME, routingKey, null, message.getBytes());
        System.out.println("消息发送完毕");
    }
}
