package lhz.route;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import lhz.RabbitMqUtils;

/**
 * @Author: LiHuaZhi
 * @Date: 2021/10/20 20:30
 * @Description:
 **/
public class Producer {

    /**
     * 定义交换机和队列名称
     */
    private static final String EXCHANGE_NAME = "direct_exchange";

    public static void main(String[] args) throws Exception {
        // 获取Channel
        Channel channel = RabbitMqUtils.getChannel();

        /*绑定的交换机 参数1交互机名称 参数2 exchange类型 */
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        // 发送消息
        String message = "", sendType = "";
        for (int i = 0; i < 20; i++) {
            if (i % 2 == 0) {
                sendType = "info";
                message = "我是 info 级别的消息类型：" + i;
            } else {
                sendType = "error";
                message = "我是 error 级别的消息类型：" + i;
            }
            System.out.println("[send]：" + message + "  " + sendType);

            // 第二个参数就是路由键
            channel.basicPublish(EXCHANGE_NAME, sendType, null, message.getBytes());
        }
        System.out.println("消息发送完毕");
    }
}
